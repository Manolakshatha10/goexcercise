package main

import (
	"fmt"
)

func main() {

	a := map[int]string{
		1: "Vijay",
		2: "kamal",
		3: "Rajini",
	}
	fmt.Println(a)
	fmt.Println(a[1])

	a[4] = "Ajith"
	for k := range a {
		fmt.Println(k, a)
	}

}
