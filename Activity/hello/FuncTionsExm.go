package main

import "fmt"

func main() {
	sample()
	foo(3, 5, 4, 2, 2, 4, 4, 5, 4)
}

func sample() {
	fmt.Println("Helllo")

}

func foo(x ...int) {
	fmt.Println(x)
	fmt.Printf("%T\n", x)
	sum := 0
	for i, v := range x {
		sum += v
		fmt.Println("values", i)
	}
	fmt.Println(sum)
}
