package main

import (
	"fmt"
)

type Student struct {
	name  string
	stdid int
	dept  string
}

func main() {

	s1 := Student{
		name:  "Arun",
		stdid: 101,
		dept:  "Ece",
	}
	s2 := Student{

		name:  "Bala",
		stdid: 102,

		dept: "Cse",
	}

	fmt.Println(s1.name)
	fmt.Println(s2)

}
