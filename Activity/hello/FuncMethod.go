package main

import "fmt"

func main() {

	a := Student{
		name: "Aravind",
		id:   101,
	}
	fmt.Println(a)
	a.speak()
}

type Student struct {
	name string
	id   int
}

func (s Student) speak() {
	fmt.Println("Hello", s.name)
}
