package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", getDetail)
	log.Fatal(http.ListenAndServe("localhost:3001", nil))

}

type user struct {
	Name     string
	Email    string
	Password string
	Phone    int
}

func hello(Wri http.ResponseWriter, Rea *http.Request) {
	fmt.Println(Wri, "hello world")
}
func getDetail(Wri http.ResponseWriter, Rea *http.Request) {
	Users := []user{
		{Name: "Arun",
			Email:    "abc@gmail.com",
			Password: "abc123",
			Phone:    1234456,
		},
		{Name: "Bala",
			Email:    "adc@gmail.com",
			Password: "a123",
			Phone:    14456},
	}
	Wri.Header().Add("Content-Type", "application/json")
	json.NewEncoder(Wri).Encode(Users)
}
