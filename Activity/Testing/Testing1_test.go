package main

import "testing"

func Testsum(t *testing.T) {
	a := addsum(3, 4)

	if a != 7 {
		t.Error("Expected ", 7, "Got ", a)
	}
}
