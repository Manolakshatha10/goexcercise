package main

import "fmt"

func main() {
	fmt.Println(addsum(3, 4))
}
func addsum(a ...int) int {
	sum := 0
	for _, v := range a {
		sum += v
	}
	return sum + 1
}
