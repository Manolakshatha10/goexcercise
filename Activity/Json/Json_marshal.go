package main

import (
	"encoding/json"
	"fmt"
)

type Employee struct {
	Name  string
	Empid int
	Desg  string
}

func main() {

	emp1 := Employee{
		Name:  " Raman",
		Empid: 101,
		Desg:  "Senior developer",
	}
	emp2 := Employee{
		Name:  " Radha",
		Empid: 102,
		Desg:  "Technical lead",
	}
	emp3 := Employee{
		Name:  " Krishna",
		Empid: 103,
		Desg:  "Junior Developer",
	}
	employee := []Employee{emp1, emp2, emp3}
	fmt.Println(employee)
	bs, err := json.Marshal(employee)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(bs))

}
