package main

import (
	"fmt"
	"sync"
)

func main() {

	var wg sync.WaitGroup
	count := 5
	wg.Add(count)

	for i := 0; i < count; i++ {
		fmt.Println("Welcome")
		wg.Done()
	}
	wg.Wait()
}
