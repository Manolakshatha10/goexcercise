package main

import "fmt"

func main() {
	even := make(chan int)
	odd := make(chan int)
	quit := make(chan int)
	go transmit(even, odd, quit)
	Receive(even, odd, quit)
}
func transmit(eve, od, qu chan<- int) {
	for i := 1; i < 10; i++ {
		if i%2 == 0 {
			eve <- i
		} else {
			od <- i
		}
	}
	qu <- 0
}

func Receive(eve, od, qu <-chan int) {
	for {
		select {
		case a := <-eve:
			fmt.Println("These from Even :", a)
		case a := <-od:
			fmt.Println("these from odd:", a)
		case a := <-qu:
			fmt.Println("from quit channeml", a)
			return

		}
	}
}
