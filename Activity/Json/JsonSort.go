package main

import (
	"fmt"
	"sort"
)

func main() {
	arr := []int{3, 4, 23, 5, 7, 9, 89, 75, 1}
	fmt.Println(" the original array:", arr)
	sort.Ints(arr)
	fmt.Println("the Sorted array:", arr)
}
