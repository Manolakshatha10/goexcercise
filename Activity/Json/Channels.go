package main

import (
	"fmt"
)

func main() {
	a := make(chan int, 1)
	a <- 42
	fmt.Println(<-a)

}
