package main

import "fmt"

type Employee struct {
	empId    int
	basicpay int
	pf       int
}
type SalaryCalculator interface {
	CalculateSalary() int
}

func (p Employee) CalculateSalary() int {
	return p.basicpay + p.pf
}
func totalExpense(s []SalaryCalculator) {
	expense := 0
	for _, v := range s {
		expense = expense + v.CalculateSalary()
	}
	fmt.Println("Total Expense Per Month :", expense)
}
func main() {
	pemp1 := Employee{
		empId:    1,
		basicpay: 5000,
		pf:       20,
	}

	employees := []SalaryCalculator{pemp1}
	fmt.Println(pemp1)
	totalExpense(employees)

}
